This folder contains all the flex pcb heater designs:

### Naming Convection

`____-____-____`

(setup)-(watts)-(dimensions of single heater)

setup:
* 2SH = 2 side heaters
* OVAH = over visible area with holes (heats the visible area of the chip with special holes for to maintain channel visibility)
* OG = over glass (covers all the glass)

Example: 2SH-4-32.5x10.5

